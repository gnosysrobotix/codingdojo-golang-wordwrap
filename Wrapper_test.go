package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWrapper_Wrap_Lorem_correctResult(t *testing.T) {
	text := "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
	expected := "Lorem ipsum\ndolor sit amet,\nconsectetur\nadipiscing\nelit, sed do\neiusmod tempor\nincididunt\nut labore et\ndolore magna\naliqua. Ut\nenim ad minim\nveniam, quis\nnostrud exercitation\nullamco laboris\nnisi ut aliquip\nex ea commodo\nconsequat.\nDuis aute irure\ndolor in reprehenderit\nin voluptate\nvelit esse\ncillum dolore\neu fugiat nulla\npariatur. Excepteur\nsint occaecat\ncupidatat non\nproident, sunt\nin culpa qui\nofficia deserunt\nmollit anim\nid est laborum."

	err, actual := Wrapper{}.Wrap(text, 10)

	assert.Nil(t, err)
	assert.Equal(t, expected, actual)

}

func TestWrapper_Wrap_4SpacedCharacters_2Lines(t *testing.T) {
	text := "A B C D"
	expected := "A B\nC D"

	err, actual := Wrapper{}.Wrap(text, 2)

	assert.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func TestWrapper_Wrap_EmptyText_EmptyText(t *testing.T) {
	text := ""
	expected := ""

	err, actual := Wrapper{}.Wrap(text, 2)

	assert.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func TestWrapper_Wrap_TextNoBreaks_Error(t *testing.T) {
	text := "ABCDEFGHIJKL"
	expected := ""

	err, actual := Wrapper{}.Wrap(text, 2)

	assert.NotNil(t, err)
	assert.Equal(t, expected, actual)
}