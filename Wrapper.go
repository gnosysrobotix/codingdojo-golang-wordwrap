package main

import (
	"errors"
	"strings"
)

/*
    Coding Dojo solution Golang for the wordwrap problem
    Typed with just 2 (but strong and fast) fingers on a cheap ass membrane keyboard
    Most lines will contain explanation in the comments on the behavior of the line of code

 ,###############################@        .:       ,:                               :,   ,`
  .##############################       ;@` '#   @#  '#   +:;;@@    +  #@    #    #, `@+ ,
   `###########################'       .@       @.     #  +     #   +  ##.   #   #
     #########################:        #        #      #` +     ,'  +  # #   #  #
      #######################.         #       `+       + +      #  +  #  #  #  #    ;;;
        @###           ####            #       `'       + +      #  +  #  '' #  #      #
        @###           ####            #        #      +. +     .+  +  #   # #  #      #
        @###           ####            :+       @`     #  +     #   +  #    ##   #     #
        @###           ####             +#  .#   @'  ,#   +..,##    +  #    ,#   `#`  '#
       #####################              ;'.      ''`    ````      `  `     `     ,';
       #####################
       #####################
       @###################@           ``````            .:`       ``````.       ,:`
        @###           ####            ########'      `######@     ######@    :#######
        @###           ####            ##########    ;#########    ######@   @#########
        @###           ####            ###    ###@   ###,   '###       ##@  ;###    ####
        @###           ####            ###     ###  ###      '##+      ##@  ###      @##
        @###           ####            ###     ,##. ###       ###      ##@  ###       ##@
        @###           ####            ###      ##; ###       ###      ##@ .##.       ###
        @###           ####            ###      ##: ##@       ###      ##@ `##:       ##@
        @###           ####            ###     ###  ###       ##@      ##@  ###      ,##;
        @###           ####            ###     ###  +##@     ###  `    ##@  @##'     ###
        @###           ####            ###..,@###    ####;.'###@  ##@:###:   ####:.####;
        @###           ####            #########      ########@   #######     ########'
        @###           ####            #######:        .####@      +####       :#####
        @###           ###@
*/

// this is the "class" wrapper. In GO we have no concept of classes but we have structs!
type Wrapper struct{}

// the function is added to the struct which we can call later
func (w Wrapper) Wrap(text string, column int) (error, string) {

	// We will test here if there is text or that the text is longer than the column width
	// if not we will return the text
	if len(text) == 0 || len(text) < column {
		return nil, text
	}

	//Here we test if the text has spaces or new lines so we can break it apart. if not we return an error
	if !strings.ContainsAny(text, " \n") {
		return errors.New("No location in text to wrap"), ""
	}

	// strings.Fields will break a string in to a slice of words
	words := strings.Fields(text)

	// declaring two string variables
	result := ""
	line := ""

	// Here we are going to iterate over the words
	for _, word := range words {

		// we add the word to the line
		line += word

		// testing if the length of the line is smaller than the coulmn width
		if len(line) < column {
			line += " "
			continue
		} else {
			//we add the line to the result
			result += line

			//we test if the result is smaller than the whole text, if it is not we add the new line, if it is we will skip the new line to get a clean end of the text
			if len(result)+1 <= len(text) {
				result += "\n"
			}

			// we reset the line
			line = ""
		}
	}

	//we return no error and the resulting text
	return nil, result
}
