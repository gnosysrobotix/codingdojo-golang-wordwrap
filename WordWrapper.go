package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {

	// we get the column width from the command args and convert it from ascii to integer
	column, err := strconv.Atoi(os.Args[1])

	// the conversion failed end we exit the program with a message to the console
	if err != nil {
		fmt.Println("Please provide a correct number the column width")
		os.Exit(-1)
	}

	text := "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

	// we call the wrap function and print the result to the console
	_, result := Wrapper{}.Wrap(text, column)
	fmt.Println(result)

}
